1 - Justificativa: 
Hilt foi escolhido para injeção de dependência, por ser a versão mais nova do dagger e recomendada pela google.
Coroutines foi escolhido para simultaneidade, por ser recomendado pela google.
Glide foi escolhido para download e cache de imagens.
Gson foi escolhido para parse de Json.

2 - O que eu faria se tivesse mais tempo.

1- Testes unitários
2- Desenvolveria o placeholder de loadind das telas
3- Melhoraria alguns detalhes de layout

3 - Instruções de como executar a aplicação

Basta clonar o projeto, não tem nenhuma configuração.
