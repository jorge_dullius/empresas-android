package com.jorge.empresas.data.entity

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Enterprise(
    @SerializedName("city")
    val city: String,
    @SerializedName("country")
    val country: String,
    @SerializedName("description")
    val description: String,
    @SerializedName("email_enterprise")
    val emailEnterprise: String,
    @SerializedName("enterprise_name")
    val enterpriseName: String,
    @SerializedName("enterprise_type")
    val enterpriseType: EnterpriseType,
    @SerializedName("facebook")
    val facebook: String,
    @SerializedName("id")
    val id: Int,
    @SerializedName("linkedin")
    val linkedin: String,
    @SerializedName("own_enterprise")
    val ownEnterprise: Boolean,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("photo")
    val photo: String,
    @SerializedName("share_price")
    val sharePrice: Double,
    @SerializedName("twitter")
    val twitter: String,
    @SerializedName("value")
    val value: Int
): Serializable