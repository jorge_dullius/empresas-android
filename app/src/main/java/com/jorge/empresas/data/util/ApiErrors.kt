package com.jorge.empresas.data.util

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.ArrayList

data class ApiErrors(
    @SerializedName("message") val errorMessage: String?,
    @SerializedName("errors") val errors: ArrayList<String>?
) : Serializable {
}