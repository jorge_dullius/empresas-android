package com.jorge.empresas.data.repository

import com.jorge.empresas.data.ApiClient
import com.jorge.empresas.data.entity.User
import com.jorge.empresas.data.util.AuthInterceptor.Companion.ACCESS_TOKEN
import com.jorge.empresas.domain.boundary.UserRepository
import com.jorge.empresas.domain.util.Cache
import javax.inject.Inject

class DefaultUserRepository @Inject constructor(
    private val apiClient: ApiClient,
    private val cache: Cache
) : UserRepository {
    override suspend fun signIn(user: User): User? {
        return apiClient.signIn(user)?.also { cacheUser(user) }
    }

    private fun cacheUser(user: User) {
        cache.set(USER, user)
    }

    fun getAccessToken(): User? {
        return try {
            cache.get(USER, User::class.java) as User
        } catch (t: Throwable) {
            null
        }
    }

    companion object {
        const val USER = "USER"
    }
}