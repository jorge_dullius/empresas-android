package com.jorge.empresas.data.util

import com.jorge.empresas.domain.RequestException
import kotlinx.coroutines.coroutineScope
import okhttp3.ResponseBody
import retrofit2.Response
import java.io.IOException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

open class RequestHandler {
    protected suspend fun <T> makeRequest(block: suspend () -> Response<T>): T? {
        return coroutineScope {
            try {
                block().run {
                    if (isSuccessful) {
                        body()
                    } else {
                        throw RequestException.HttpError(code(), extractErrorBody(errorBody()))
                    }
                }
            } catch (t: Exception) {
                throw when (t) {
                    is RequestException -> t
                    is SocketTimeoutException -> RequestException.TimeoutError()
                    is UnknownHostException -> RequestException.NetworkError()
                    is IOException -> RequestException.NetworkError()
                    else -> RequestException.UnexpectedError(t)
                }
            }
        }
    }

    private fun extractErrorBody(errorBody: ResponseBody? = null): String? {
        return ApiErrorsFormatter.deserialize(errorBody)?.let {
            if (it.errors != null) {
                it.errors.joinToString("\n")
            } else {
                it.errorMessage
            }
        }
    }
}