package com.jorge.empresas.data

import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.data.entity.Enterprises
import com.jorge.empresas.data.entity.User
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @POST("api/v1/users/auth/sign_in")
    suspend fun signIn(@Body user: User): Response<User>

    @GET("api/v1/enterprises")
    suspend fun getEnterprisesByName(@Query("name") name: String): Response<Enterprises>

    @GET("api/v1/enterprises/{id}")
    suspend fun showEnterprise(@Path("id")  id: Long): Response<Enterprise>
}