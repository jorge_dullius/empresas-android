package com.jorge.empresas.data.util

import com.google.gson.Gson
import okhttp3.ResponseBody

object ApiErrorsFormatter {
    fun deserialize(responseBody: ResponseBody?): ApiErrors? = Gson().fromJson(
        responseBody?.string(),
        ApiErrors::class.java
    )
}