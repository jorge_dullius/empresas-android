package com.jorge.empresas.data.entity


import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EnterpriseType(
    @SerializedName("enterprise_type_name")
    val enterpriseTypeName: String,
    @SerializedName("id")
    val id: Int
): Serializable