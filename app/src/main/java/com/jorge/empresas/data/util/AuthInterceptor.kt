package com.jorge.empresas.data.util

import com.jorge.empresas.domain.util.Cache
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class AuthInterceptor @Inject constructor(private val cache: Cache) : Interceptor {

    companion object {
        const val USER = "USER"
        const val ACCESS_TOKEN = "access-token"
        const val UID = "uid"
        const val CLIENT = "client"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        try {
            requestBuilder.addHeader(ACCESS_TOKEN, getAccessTokenFromCache())
            requestBuilder.addHeader(UID, getUIDFromCache())
            requestBuilder.addHeader(CLIENT, getClientFromCache())
        } catch (e: Cache.NotFoundException) {
            /* Nothing to do here */
        }

        val response = chain.proceed(requestBuilder.build())
        response.headers.get(ACCESS_TOKEN)?.let { cache.set(ACCESS_TOKEN, it) }
        response.headers.get(UID)?.let { cache.set(UID, it) }
        response.headers.get(CLIENT)?.let { cache.set(CLIENT, it) }
        return response
    }

    private fun getAccessTokenFromCache() = cache.get<String>(ACCESS_TOKEN, String::class.java)
    private fun getUIDFromCache() = cache.get<String>(UID, String::class.java)
    private fun getClientFromCache() = cache.get<String>(CLIENT, String::class.java)
}