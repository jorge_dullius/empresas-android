package com.jorge.empresas.data.entity

import com.google.gson.annotations.SerializedName

data class Enterprises(
    @SerializedName("enterprises")
    val enterprisesList: List<Enterprise>,
)