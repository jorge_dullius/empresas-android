package com.jorge.empresas.data.repository

import com.jorge.empresas.data.ApiClient
import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.data.entity.Enterprises
import com.jorge.empresas.domain.boundary.EnterpriseRepository
import javax.inject.Inject

class DefaultEnterpriseRepository @Inject constructor(
    private val apiClient: ApiClient
) : EnterpriseRepository {
    override suspend fun getEnterprisesByName(name: String): Enterprises? {
        return apiClient.getEnterprisesByName(name)
    }

    override suspend fun showEnterprise(id: Long): Enterprise? {
        return apiClient.showEnterprise(id)
    }
}