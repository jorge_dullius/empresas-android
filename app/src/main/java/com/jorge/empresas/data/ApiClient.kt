package com.jorge.empresas.data

import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.data.entity.Enterprises
import com.jorge.empresas.data.entity.User
import com.jorge.empresas.data.util.RequestHandler
import javax.inject.Inject

class ApiClient @Inject constructor(
    private val apiService: ApiService
) : RequestHandler() {
    suspend fun signIn(user: User): User? {
        return makeRequest { apiService.signIn(user) }
    }

    suspend fun getEnterprisesByName(name: String): Enterprises? {
        return makeRequest { apiService.getEnterprisesByName(name) }
    }

    suspend fun showEnterprise(id: Long): Enterprise? {
        return makeRequest { apiService.showEnterprise(id) }
    }
}