package com.jorge.empresas.domain.boundary

import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.data.entity.Enterprises

interface EnterpriseRepository {
    suspend fun getEnterprisesByName(name: String): Enterprises?
    suspend fun showEnterprise(id: Long): Enterprise?
}