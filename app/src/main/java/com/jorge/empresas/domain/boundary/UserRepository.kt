package com.jorge.empresas.domain.boundary

import com.jorge.empresas.data.entity.User

interface UserRepository {
    suspend fun signIn(user: User): User?
}