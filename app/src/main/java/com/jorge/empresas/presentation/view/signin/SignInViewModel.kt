package com.jorge.empresas.presentation.view.signin

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jorge.empresas.data.entity.User
import com.jorge.empresas.data.repository.DefaultUserRepository
import com.jorge.empresas.domain.RequestException
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SignInViewModel @Inject constructor(
    private val userRepository: DefaultUserRepository
) : ViewModel() {

    val dialog: LiveData<String> get() = _dialog

    private val _dialog by lazy { MutableLiveData<String>() }

    val goToMain: LiveData<Unit> get() = _goToMain

    private val _goToMain by lazy { MutableLiveData<Unit>() }

    init {
        if(userRepository.getAccessToken() != null){
            _goToMain.postValue(Unit)
        }
    }

    fun signIn(email: String, password: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                userRepository.signIn(
                    User(
                        email,
                        password
                    )
                )
                _goToMain.postValue(Unit)
            } catch (exception: Exception) {
                if (exception is RequestException.HttpError) {
                    _dialog.postValue(exception.errorMessage)
                } else {
                    _dialog.postValue("Um erro inesperado aconteceu!")
                }

            }
        }
    }
}