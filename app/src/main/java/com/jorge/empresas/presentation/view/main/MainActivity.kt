package com.jorge.empresas.presentation.view.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.jorge.empresas.R
import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var searchView: SearchView

    private val viewModel: MainViewModel by viewModels()

    private val adapter by lazy { EnterprisesListAdapter(::openEnterpriseDetailsFragment) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setSupportActionBar(binding.topAppBar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setContentView(binding.root)
        setupUi()
        subscribeUi()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.top_app_bar, menu)
        setupSearchView(menu?.findItem(R.id.search_menu_item))
        return super.onCreateOptionsMenu(menu)
    }

    private fun setupUi() {
        binding.enterprisesList.layoutManager = LinearLayoutManager(
            this,
            LinearLayoutManager.VERTICAL,
            false
        )
        binding.enterprisesList.adapter = adapter
    }

    private fun subscribeUi() {
        viewModel.dialog.observe(this, {
            MaterialAlertDialogBuilder(this).setMessage(it).setTitle("Oops").show()
        })
        viewModel.enterprises.observe(this, ::onEnterprises)
    }

    private fun onEnterprises(enterprises: List<Enterprise>) {
        if (enterprises.isEmpty()) {
            binding.noResultText.visibility = View.VISIBLE
            adapter.submitList(null)
        } else {
            binding.noResultText.visibility = View.GONE
            adapter.submitList(enterprises)
        }
    }

    private fun openEnterpriseDetailsFragment(enterprise: Enterprise) {
        startActivity(EnterpriseDetailsActivity.createIntent(this, enterprise))
    }

    private fun setupSearchView(searchItem: MenuItem?) {
        searchItem?.let {
            it.setOnActionExpandListener(
                SearchExpandHelper(
                    onSearchExpand = ::onSearchViewExpand,
                    onSearchCollapse = ::onSearchViewCollapse
                )
            )
            searchView = it.actionView as SearchView
            searchView.setOnQueryTextListener(SearchHelper(viewModel::getEnterprisesByName))
            searchView.setIconifiedByDefault(true)
        }
    }

    private fun onSearchViewExpand() {
        binding.placeholderText.visibility = View.GONE
    }

    private fun onSearchViewCollapse() {
        binding.placeholderText.visibility = View.VISIBLE
        binding.noResultText.visibility = View.GONE
        adapter.submitList(null)
    }

    companion object {
        fun createIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}