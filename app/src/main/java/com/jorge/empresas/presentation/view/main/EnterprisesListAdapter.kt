package com.jorge.empresas.presentation.view.main

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.jorge.empresas.data.entity.Enterprise

class EnterprisesListAdapter(
    private val onItemClicked: (Enterprise) -> Unit
) : ListAdapter<Enterprise, EnterpriseViewHolder>(DiffUtilCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EnterpriseViewHolder {
        return EnterpriseViewHolder.inflate(parent)
    }

    override fun onBindViewHolder(holder: EnterpriseViewHolder, position: Int) {
        holder.bind(getItem(position), onItemClicked)
    }

    companion object DiffUtilCallback : DiffUtil.ItemCallback<Enterprise>() {
        override fun areItemsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: Enterprise, newItem: Enterprise): Boolean =
            oldItem == newItem
    }
}
