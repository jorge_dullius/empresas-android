package com.jorge.empresas.presentation.view.signin

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.jorge.empresas.databinding.ActivitySignInBinding
import com.jorge.empresas.presentation.view.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SignInActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySignInBinding
    private val viewModel: SignInViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignInBinding.inflate(layoutInflater)
        setupUi()
        subscribeUi()
        setContentView(binding.root)
    }

    private fun subscribeUi() {
        viewModel.goToMain.observe(this, {
            startActivity(MainActivity.createIntent(this))
        })
        viewModel.dialog.observe(this, {
            binding.emailTextInput.error = it
            binding.passwordTextInput.error = it
            binding.signInInvalidCredentialsError.visibility = View.VISIBLE
            binding.submitButton.isEnabled = false
        })
    }

    private fun setupUi() {
        binding.submitButton.setOnClickListener {
            viewModel.signIn(
                binding.emailTextInput.text.toString(),
                binding.passwordTextInput.text.toString()
            )
        }
        binding.emailTextInput.doOnTextChanged { _, _, _, _ -> removeErrors() }
    }

    private fun removeErrors() {
        binding.emailTextInput.error = null
        binding.passwordTextInput.error = null
        binding.signInInvalidCredentialsError.visibility = View.GONE
        binding.submitButton.isEnabled = true
    }
}