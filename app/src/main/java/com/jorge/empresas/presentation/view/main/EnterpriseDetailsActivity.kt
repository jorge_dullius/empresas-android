package com.jorge.empresas.presentation.view.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.databinding.ActivityEnterpriseDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class EnterpriseDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEnterpriseDetailsBinding

    val enterprise: Enterprise by lazy { intent?.extras?.getSerializable(ENTERPRISE_EXTRA) as Enterprise }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEnterpriseDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.topAppBar)
        setupUi()
    }

    private fun setupUi() {
        val requestOptions = RequestOptions().fitCenter()
        Glide.with(binding.root.context).load("https://empresas.ioasys.com.br/" + enterprise.photo)
            .apply(requestOptions)
            .into(binding.enterpriseImageView)
        binding.enterpriseDescription.text = enterprise.description
        binding.topAppBar.title = enterprise.enterpriseName
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val ENTERPRISE_EXTRA = "ENTERPRISE_EXTRA"
        fun createIntent(context: Context, enterprise: Enterprise): Intent {
            return Intent(context, EnterpriseDetailsActivity::class.java).apply {
                putExtra(ENTERPRISE_EXTRA, enterprise)
            }
        }
    }
}