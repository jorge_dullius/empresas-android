package com.jorge.empresas.presentation.view.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.data.repository.DefaultEnterpriseRepository
import com.jorge.empresas.domain.RequestException
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val defaultEnterpriseRepository: DefaultEnterpriseRepository
) : ViewModel() {
    val dialog: LiveData<String> get() = _dialog

    private val _dialog by lazy { MutableLiveData<String>() }

    val enterprises: LiveData<List<Enterprise>> get() = _enterprises

    private val _enterprises by lazy { MutableLiveData<List<Enterprise>>() }

    fun getEnterprisesByName(search: String) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                defaultEnterpriseRepository.getEnterprisesByName(
                    search
                ).let {
                    _enterprises.postValue(it?.enterprisesList)
                }
            } catch (exception: Exception) {
                if (exception is RequestException.HttpError) {
                    _dialog.postValue(exception.errorMessage)
                } else {
                    _dialog.postValue("Um erro inesperado aconteceu!")
                }
            }
        }
    }
}