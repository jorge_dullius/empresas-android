package com.jorge.empresas.presentation.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.jorge.empresas.data.entity.Enterprise
import com.jorge.empresas.databinding.ItemCellEnterprisesBinding

class EnterpriseViewHolder(
    private val binding: ItemCellEnterprisesBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(enterprise: Enterprise, onContractClickedCallback: (Enterprise) -> Unit) {
        binding.enterpriseName.text = enterprise.enterpriseName
        binding.enterpriseType.text = enterprise.enterpriseType.enterpriseTypeName
        binding.enterpriseCountry.text = enterprise.country
        val requestOptions = RequestOptions().centerCrop()
        Glide.with(binding.root.context).load("https://empresas.ioasys.com.br/" + enterprise.photo).apply(requestOptions)
            .into(binding.enterpriseImageView)
        binding.root.setOnClickListener { onContractClickedCallback(enterprise) }
    }

    companion object {

        fun inflate(parent: ViewGroup) = EnterpriseViewHolder(
            ItemCellEnterprisesBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
}