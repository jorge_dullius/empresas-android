package com.jorge.empresas.presentation.view.main

import android.view.MenuItem

class SearchExpandHelper(
    private val onSearchExpand: ()-> Unit,
    private val onSearchCollapse: ()-> Unit
): MenuItem.OnActionExpandListener {
    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        onSearchExpand()
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        onSearchCollapse()
        return true
    }
}