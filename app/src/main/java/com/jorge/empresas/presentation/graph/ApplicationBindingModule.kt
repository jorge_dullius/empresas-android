package com.jorge.empresas.presentation.graph

import com.jorge.empresas.data.entity.PreferencesCache
import com.jorge.empresas.data.repository.DefaultEnterpriseRepository
import com.jorge.empresas.data.repository.DefaultUserRepository
import com.jorge.empresas.data.util.AuthInterceptor
import com.jorge.empresas.domain.boundary.EnterpriseRepository
import com.jorge.empresas.domain.boundary.UserRepository
import com.jorge.empresas.domain.util.Cache
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import dagger.multibindings.IntoSet
import okhttp3.Interceptor

@Module
@InstallIn(SingletonComponent::class)
interface ApplicationBindingModule {

    @Binds
    fun bindUserRepository(repository: DefaultUserRepository): UserRepository

    @Binds
    fun bindEnterpriseRepository(repository: DefaultEnterpriseRepository): EnterpriseRepository

    @Binds
    @IntoSet
    fun bindAuthInterceptor(interceptor: AuthInterceptor): Interceptor

    @Binds
    fun bindCache(cache: PreferencesCache): Cache
}