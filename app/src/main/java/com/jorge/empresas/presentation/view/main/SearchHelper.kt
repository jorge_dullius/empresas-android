package com.jorge.empresas.presentation.view.main

import androidx.appcompat.widget.SearchView

class SearchHelper(private val callback: (String) -> Unit) : SearchView.OnQueryTextListener {
    override fun onQueryTextSubmit(p0: String?): Boolean {
        p0?.let { callback(it) }
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {
//        p0?.let { callback(it) }
        return true
    }
}