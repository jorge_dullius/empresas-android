package com.jorge.empresas.presentation

import android.app.Application
import com.jorge.empresas.data.entity.PreferencesCache
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class EmpresasApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        PreferencesCache.init(this)
    }

}